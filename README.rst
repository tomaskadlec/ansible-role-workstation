ansible-role-workstation
========================

This project is a set of Ansible roles that allows to install and manage several
workstations/notebooks running Debian GNU/Linux in a consistent way. Some roles
are not Debian specific and may be used in general.

Requirements
------------

.. _Preseed: https://wiki.debian.org/DebianInstaller/Preseed
.. _Cobbler: http://cobbler.github.io/

The only requirement is to have base system installed, ssh running and ansible
shold be able to log in as the ``root`` user. Use another tool to install base
system, e.g. using Preseed_ or Cobbler_.

Roles
-----

Following list of roles matches structure of the project. Please refer to the
documentation of each role how to use it.

.. _debian: debian/README.rst
.. _displaylink: displaylink/README.rst   

debian_
    Configures apt with several other repos (including backports), handles updating
    apt cache and upgrading the whole system. It installs commonly needed tools and
    Cinnamon desktop environment.

displaylink_
    Installs or upgrades displaylink driver and allows to use display devices based
    on DisplayLink chipsets.
    
