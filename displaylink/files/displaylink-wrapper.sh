#!/bin/bash

function start {
    systemctl start displaylink
    for i in 1 2; do xrandr --setprovideroutputsource $i 0; done
}

function stop {
    systemctl stop displaylink
}

function help {
    cat 1>&2 <<- --USAGE--
Usage: $0 start|stop

    start   starts displaylink service, register providers
    stop    stops displaylink service (thus disconnects monitors)

--USAGE--
}

case "$1" in
    'start' )
        start
        ;;
    'stop' )
        stop
        ;;
    *)
        echo "[ERROR] Unsupported operation." 1>&2
        help
        exit 1
        ;;
esac

